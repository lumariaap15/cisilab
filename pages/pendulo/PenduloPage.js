import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity,
    Platform, Text
} from 'react-native';
import {Icon, Button, Header, Input, Overlay} from 'react-native-elements';
import {connect} from "react-redux";
import PenduloFormulas from "./PenduloFormulas";
import {MODULO_CIRCULAR, MODULO_PENDULO, MODULO_PLANO} from "../../helpers/Globals";
import AwesomeAlert from "react-native-awesome-alerts";

const LeftComponent = (action) => {
    return (
        <TouchableOpacity onPress={action}>
        <Image
            style={{ width: 60, height: 70, resizeMode:'contain' }}
            source={require('../../assets/img/logo-sm.png')}
        />
        </TouchableOpacity>
    );
};

const RightComponent = action => {
    return (
        <Image
            style={{ width: 50, height: 60, resizeMode: 'contain' }}
            source={require('../../assets/img/blue/pendulo.png')}
        />
    );
};

class PenduloPage extends Component {
    state = {
        loading:false,
        mostrarFormulas: false,
        showAlert: false,
        messageAlert:'',
        actionAlert:'',
        data:{
            //oscilaciones:'',
            longitud:''
        }
    };

    cerrarFormulas(){
        this.setState({mostrarFormulas: false})
    }

    getResultado(){
        this.setState({loading: true});
        let longitud = Number(this.state.data.longitud)
        let periodo = Number(this.props.WS.message.segundos+'.'+this.props.WS.message.milisegundos)
        this.props.navigation.navigate('PenduloResultado',{periodo: periodo, longitud: longitud});
        this.setState({loading: false});
    }

    componentDidMount(): void {
        if (this.props.WS.message.modulo !== MODULO_PENDULO){
            let modulo = '';
            let page = '';
            if(this.props.WS.message.modulo === MODULO_PLANO){
                modulo = 'Plano inclinado'
                page = 'PlanoPage'
            }else if(this.props.WS.message.modulo === MODULO_CIRCULAR){
                modulo = 'Movimiento Circular Uniforme'
                page = 'MovimientoCircularPage'
            }
            if(modulo !== '') {
                this.setState({
                    showAlert: true,
                    messageAlert: 'Te encuentras en el módulo equivocado, por favor ve a ' + modulo,
                    actionAlert: page
                })
            }else{
                this.setState({
                    showAlert: true,
                    messageAlert: 'Error al conectarse a la red de CISCELAB',
                    actionAlert: ''
                })
            }
        }
    }


    render() {
        return (
            <View style={{position:"relative", minHeight:'100%'}}>
                <Overlay isVisible={this.state.mostrarFormulas}>
                    <PenduloFormulas cerrar={()=>{this.cerrarFormulas()}} />
                </Overlay>
                <AwesomeAlert
                    show={this.state.showAlert}
                    title="Error!"
                    message={this.state.messageAlert}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={this.state.actionAlert !== ''}
                    confirmText="Ir al módulo"
                    confirmButtonColor="#4585BA"
                    cancelText="Volver a inicio"
                    alertContainerStyle={{zIndex:10}}
                    onConfirmPressed={() => {
                        this.props.navigation.navigate(this.state.actionAlert);
                    }}
                    onCancelPressed={()=>{
                        this.props.navigation.navigate('MainPage');
                    }}
                />
                <Header
                    backgroundColor="#fff"
                    leftComponent={LeftComponent(()=>{
                        //this.props.WS.ws.removeEventListener('message');
                        this.props.navigation.navigate('MainPage')})
                    }
                    centerComponent={{ text: 'PÉNDULO SIMPLE', style:{color: '#4585BA', fontSize:18, fontFamily:'Poppins-Bold'} }}
                    rightComponent={RightComponent}
                    containerStyle={{
                        height: Platform.OS === 'ios' ? 110 : 70,
                        paddingTop:0,
                        borderBottomWidth:3,
                        borderBottomColor:"#EFEFEF"
                    }}
                />
                <ScrollView style={{padding:10, flex: 1}}>
                    <View>
                        <View style={{justifyContent:'space-between',flexDirection:"row", alignItems:"center", marginBottom:20}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon
                                    name='arrow-back'
                                    color='black'
                                />
                            </TouchableOpacity>
                            <Text style={{fontSize:18, fontFamily:'Poppins-Bold'}}>INGRESA LOS DATOS</Text>
                            <TouchableOpacity onPress={()=>{this.setState({mostrarFormulas: true})}}>
                                <View style={styles.buttonHelp}>
                                    <Text style={{color:"#FD9000",fontSize:22, textAlign:"center", fontFamily:"Poppins-Regular"}}>?</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            {/*
                            <Input
                                label="Oscilaciones:"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='NÚMERO DE OSCILACIONES'
                                value={this.state.data.oscilaciones}
                                onChangeText={val => this.setState(prevState => ({ data: { ...prevState.data, oscilaciones: val } }))}
                            />
                            */}
                            <Input
                                label="Longitud:"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='LONGITUD (METROS)'
                                value={this.state.data.longitud}
                                keyboardType="decimal-pad"
                                defaultValue="0"
                                onChangeText={val => this.setState(prevState => ({ data: { ...prevState.data, longitud: val } }))}
                            />
                            <View style={{paddingLeft:7, paddingRight:7}}>
                                <Button
                                    buttonStyle={{borderColor:"#FD9000", backgroundColor:"#FD9000", borderRadius: 100}}
                                    titleStyle={{color:"#fff", fontFamily:"Poppins-Bold"}}
                                    title="CAPTURAR"
                                    type="outline"
                                    loading={this.state.loading}
                                    loadingProps={{color:"#fff"}}
                                    onPress={()=>{this.getResultado()}}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                {/*
                <View style={{position:"absolute", bottom:-50, right:0, zIndex:-3}}>
                    <Image style={{ width: 300, height: 250, resizeMode: 'contain' }} source={require('../../assets/img/triangulo-azul.png')} />
                </View>
                */}
            </View >
        );
    }
}

const styles = StyleSheet.create({

    input:{
        backgroundColor:'#EDEDED',fontFamily:'Poppins-Bold', fontSize: 16, paddingBottom:10,paddingLeft:15, paddingRight:15, paddingTop:10,
        borderRadius: 100
    },
    container:{
        borderBottomWidth:0
    },
    superContainer:{
        marginBottom: 15
    },
    buttonHelp:{
        borderRadius:200,borderColor:"#FD9000",borderWidth:2,borderStyle:"solid", width:40, height: 40, flexDirection: "row", alignItems:"center", justifyContent: "center"
    }

});


const mapStateToProps = state => {
    return {
        WS: state.reducerWS
    };
};


export default connect(mapStateToProps)(PenduloPage)




