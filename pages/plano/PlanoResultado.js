import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity,
    Platform,
    Text
} from 'react-native';
import {Button, Header, Input, Icon, Overlay} from 'react-native-elements';
import PlanoFormulas from "./PlanoFormulas";
import {EventRegister} from "react-native-event-listeners";
import HTML from "react-native-render-html";
import AwesomeAlert from "react-native-awesome-alerts";

const LeftComponent = (action) => {
    return (
        <TouchableOpacity onPress={action}>
            <Image
                style={{ width: 60, height: 70, resizeMode:'contain' }}
                source={require('../../assets/img/logo-sm.png')}
            />
        </TouchableOpacity>
    );
}

const RightComponent = action => {
    return (
        <Image
            style={{ width: 50, height: 60, resizeMode: 'contain' }}
            source={require('../../assets/img/blue/plano.png')}
        />
    );
}

export default class PlanoResultado extends Component {
    state = {
        loading:false,
        mostrarResultado: false,
        mostrarFormulas: false,
        velocidadResultado:'',
        aceleracionResultado:'',
        velocidadUsuario:'',
        aceleracionUsuario:'',
        htmlContent:``,
        showAlert: false,
    };

    mostrarResultado(){
        this.setState({loading: true});
        let grado = this.props.navigation.getParam('grado');
        let peso = this.props.navigation.getParam('peso');
        let tiempo = this.props.navigation.getParam('tiempo');
        if(isNaN(Number(tiempo)) || Number(tiempo) === 0){
            this.setState({
                showAlert: true
            })
        }else{
            let aceleracion = Math.sin(Number(grado)) * Number(peso);
            let velocidad = aceleracion * Number(tiempo);
            let htmlContent = `
        <p>
        a = m*g*sen(&#952;)<br>
        &#952; = ${grado}°<br>
        PESO = ${peso}N = m*g<br>
        a = (${peso}N)(sen(${grado}°)) = ${(peso * Math.sin(grado)).toFixed(2)}<br>
        V = V<sub>0</sub> + at<br>
        t = ${tiempo}s<br>
        V = 0 + (${(peso * Math.sin(grado)).toFixed(2)} * ${tiempo}s)<br>
        V = ${((peso * Math.sin(grado)) * tiempo).toFixed(2)}m/s
         `
            let cincoPorcientoVelocidad = (5*velocidad)/100;
            let cincoPorcientoAceleracion = (5*aceleracion)/100;
            let margenErrorVelocidad = Math.abs(velocidad-this.state.velocidadUsuario).toFixed(2);
            let margenErrorAceleracion = Math.abs(aceleracion-this.state.aceleracionUsuario).toFixed(2);
            let messageMargen = margenErrorVelocidad+' en la velocidad y '+margenErrorAceleracion+' en la aceleración'
            if((margenErrorAceleracion<=cincoPorcientoAceleracion)&&(margenErrorVelocidad<=cincoPorcientoVelocidad)){
                //le quedó bien
                EventRegister.emit('customAlert', {type:'success',margenError:messageMargen})
            }else{
                //le quedó mal
                EventRegister.emit('customAlert', {type: 'error',margenError:messageMargen})
            }
            this.setState({
                loading: false,
                velocidadResultado:velocidad.toString(),
                aceleracionResultado:aceleracion.toString(),
                mostrarResultado: true,
                htmlContent: htmlContent
            });
        }
    }
    cerrarFormulas(){
        this.setState({mostrarFormulas: false})
    }

    render() {
        return (
            <View style={{position:"relative", minHeight:'100%'}}>
                <Overlay isVisible={this.state.mostrarFormulas}>
                    <PlanoFormulas cerrar={()=>{this.cerrarFormulas()}} />
                </Overlay>
                <AwesomeAlert
                    show={this.state.showAlert}
                    title="Error!"
                    message="Por favor, intenta de nuevo."
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText="Cerrar"
                    alertContainerStyle={{zIndex:10}}
                    onCancelPressed={()=>{
                        this.setState({
                            showAlert: false
                        });
                        this.props.navigation.goBack()
                    }}
                />
                <Header
                    backgroundColor="#fff"
                    leftComponent={LeftComponent(()=>{this.props.navigation.navigate('MainPage')})}
                    centerComponent={{ text: 'PLANO INCLINADO', style:{color: '#4585BA', fontSize:18, fontFamily:'Poppins-Bold'} }}
                    rightComponent={RightComponent}
                    containerStyle={{
                        height: Platform.OS === 'ios' ? 110 : 70,
                        paddingTop:0,
                        borderBottomWidth:3,
                        borderBottomColor:"#EFEFEF"
                    }}
                />
                <ScrollView style={{flex: 1}}>
                    <View>
                        <View style={{justifyContent:'space-between',flexDirection:"row", alignItems:"center", marginBottom:2, padding: 10}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon
                                    name='arrow-back'
                                    color='black'
                                />
                            </TouchableOpacity>
                            <Text style={{fontSize:18, fontFamily:'Poppins-Bold'}}>RESULTADO</Text>
                            <TouchableOpacity onPress={()=>{this.setState({mostrarFormulas: true})}}>
                                <View style={styles.buttonHelp}>
                                    <Text style={{color:"#FD9000",fontSize:22, textAlign:"center", fontFamily:"Poppins-Regular"}}>?</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.title}>
                            <Text style={{color:"#4585BA",fontFamily:'Poppins-Bold', fontSize:16}}>DATOS CALCULADOS POR EL USUARIO: </Text>
                        </View>
                        <View style={{paddingLeft: 10, paddingRight: 10}}>
                            <Input
                                label="Velocidad (m/s):"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='Ingresar velocidad calculada'
                                disabled={this.state.mostrarResultado}
                                value={this.state.velocidadUsuario}
                                keyboardType="decimal-pad"
                                defaultValue="0"
                                onChangeText={(text => this.setState({velocidadUsuario: text}))}
                            />
                            <Input
                                label="Aceleración (m/s2):"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='Ingresar aceleración calculada'
                                value={this.state.aceleracionUsuario}
                                disabled={this.state.mostrarResultado}
                                keyboardType="decimal-pad"
                                defaultValue="0"
                                onChangeText={(text => this.setState({aceleracionUsuario: text}))}
                            />
                        </View>
                        {
                            this.state.mostrarResultado ?
                                <View>
                                    <View style={styles.title}>
                                        <Text style={{color:"#4585BA",fontFamily:'Poppins-Bold', fontSize:16}}>DATOS OBTENIDOS: </Text>
                                    </View>
                                    <View style={{paddingLeft: 10, paddingRight: 10}}>
                                        <Input
                                            label="Velocidad (m/s):"
                                            labelProps={{fontFamily:'Poppins-Bold'}}
                                            labelStyle={{color:'black', fontSize:16}}
                                            inputStyle={styles.input}
                                            inputContainerStyle={styles.container}
                                            containerStyle={styles.superContainer}
                                            value={this.state.velocidadResultado}
                                            disabled={true}
                                        />
                                        <Input
                                            label="Aceleración (m/s2):"
                                            labelProps={{fontFamily:'Poppins-Bold'}}
                                            labelStyle={{color:'black', fontSize:16}}
                                            inputStyle={styles.input}
                                            inputContainerStyle={styles.container}
                                            containerStyle={styles.superContainer}
                                            value={this.state.aceleracionResultado}
                                            disabled={true}
                                        />
                                    </View>
                                    <HTML baseFontStyle={{fontSize:18,fontFamily:'Poppins-Regular'}} html={this.state.htmlContent} />
                                </View>
                                : <View style={{paddingLeft:17, paddingRight:17}}>
                                    <Button
                                        buttonStyle={{borderColor:"#FD9000", backgroundColor:"#FD9000",
                                        borderRadius: 100}}
                                        titleStyle={{color:"#fff", fontFamily:"Poppins-Bold"}}
                                        title="COMPARAR"
                                        type="outline"
                                        loading={this.state.loading}
                                        loadingProps={{color:"#fff"}}
                                        onPress={()=>{this.mostrarResultado()}}
                                    />
                                </View>
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    input:{
        backgroundColor:'#EDEDED',fontFamily:'Poppins-Bold', fontSize: 16, paddingBottom:10,paddingLeft:15, paddingRight:15, paddingTop:10,
         borderRadius: 100
    },
    container:{
        borderBottomWidth:0
    },
    superContainer:{
        marginBottom: 15
    },
    buttonHelp:{
        borderRadius:200,borderColor:"#FD9000",borderWidth:2,borderStyle:"solid", width:40, height: 40, flexDirection: "row", alignItems:"center", justifyContent: "center"
    },
    title:{
        backgroundColor:"#B8DFFF",paddingTop:10, paddingBottom:10, paddingLeft:15, paddingRight:15, marginBottom: 15
    }

});




