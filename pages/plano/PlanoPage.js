import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity,
    Platform,
    Text
} from 'react-native';
import { Button, Header, Input, Icon, Overlay } from 'react-native-elements';
import PlanoFormulas from "./PlanoFormulas";
import {connect} from "react-redux";
import AwesomeAlert from 'react-native-awesome-alerts';
import {MODULO_CIRCULAR, MODULO_PENDULO, MODULO_PLANO} from "../../helpers/Globals";

const LeftComponent = (action) => {
    return (
        <TouchableOpacity onPress={action}>
            <Image
                style={{ width: 60, height: 70, resizeMode:'contain' }}
                source={require('../../assets/img/logo-sm.png')}
            />
        </TouchableOpacity>
    );
}

const RightComponent = action => {
    return (
        <Image
            style={{ width: 50, height: 60, resizeMode: 'contain' }}
            source={require('../../assets/img/blue/plano.png')}
        />
    );
}

class PlanoPage extends Component {
    state = {
        loading:false,
        mostrarFormulas: false,
        data:{
            grado:'',
            distancia:'',
            peso:''
        },
        showAlert: false,
        messageAlert:'',
        actionAlert:''

    };

    cerrarFormulas(){
        this.setState({mostrarFormulas: false})
    }

    getResultado(){
        this.setState({loading: true});
        let tiempo = Number(this.props.WS.message.segundos+'.'+this.props.WS.message.milisegundos)
        this.props.navigation.navigate('PlanoResultado',{tiempo:tiempo, ...this.state.data});
        this.setState({loading: false});
    }

    componentDidMount(): void {
        if (this.props.WS.message.modulo !== MODULO_PLANO){
            let modulo = '';
            let page = '';
            if(this.props.WS.message.modulo === MODULO_PENDULO){
                modulo = 'Péndulo Simple'
                page = 'PenduloPage'
            }else if(this.props.WS.message.modulo === MODULO_CIRCULAR){
                modulo = 'Movimiento Circular Uniforme'
                page = 'MovimientoCircularPage'
            }
            if(modulo !== '') {
                this.setState({
                    showAlert: true,
                    messageAlert: 'Te encuentras en el módulo equivocado, por favor ve a ' + modulo,
                    actionAlert: page
                })
            }else{
                this.setState({
                    showAlert: true,
                    messageAlert: 'Error al conectarse a la red de CISCELAB',
                    actionAlert: ''
                })
            }
        }
    }

    render() {
        return (
            <View style={{position:"relative", minHeight:'100%'}}>
                <AwesomeAlert
                    show={this.state.showAlert}
                    title="Error!"
                    message={this.state.messageAlert}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={this.state.actionAlert !== ''}
                    confirmText="Ir al módulo"
                    confirmButtonColor="#4585BA"
                    cancelText="Volver a inicio"
                    alertContainerStyle={{zIndex:10}}
                    onConfirmPressed={() => {
                        this.props.navigation.navigate(this.state.actionAlert);
                    }}
                    onCancelPressed={()=>{
                        this.props.navigation.navigate('MainPage');
                    }}
                />
                <Overlay isVisible={this.state.mostrarFormulas}>
                    <PlanoFormulas cerrar={()=>{this.cerrarFormulas()}} />
                </Overlay>
                <Header
                    backgroundColor="#FFF"
                    leftComponent={LeftComponent(()=>{this.props.navigation.navigate('MainPage')})}
                    centerComponent={{ text: 'PLANO INCLINADO', style:{color: '#4585BA', fontSize:18, fontFamily:'Poppins-Bold'} }}
                    rightComponent={RightComponent}
                    containerStyle={{
                        height: Platform.OS === 'ios' ? 110 : 70,
                        paddingTop:0,
                        borderBottomWidth:3,
                        borderBottomColor:"#EFEFEF"
                    }}
                />
                <ScrollView style={{padding:10, flex: 1}}>
                    <View>
                        <View style={{justifyContent:'space-between',flexDirection:"row", alignItems:"center", marginBottom:20}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon
                                    name='arrow-back'
                                    color='black'
                                />
                            </TouchableOpacity>
                            <Text style={{fontSize:18, fontFamily:'Poppins-Bold'}}>INGRESA LOS DATOS</Text>
                            <TouchableOpacity onPress={()=>{this.setState({mostrarFormulas: true})}}>
                                <View style={styles.buttonHelp}>
                                    <Text style={{color:"#FD9000",fontSize:22, textAlign:"center", fontFamily:"Poppins-Regular"}}>?</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Input
                                label="Grado de inclinación:"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='GRADO DE INCLINACIÓN (°)'
                                value={this.state.data.grado}
                                keyboardType="decimal-pad"
                                defaultValue="0"
                                onChangeText={val => this.setState(prevState => ({ data: { ...prevState.data, grado: val } }))}
                            />
                            <Input
                                label="Distancia:"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='DISTANCIA (METROS)'
                                value={this.state.data.distancia}
                                keyboardType="decimal-pad"
                                defaultValue="0"
                                onChangeText={val => this.setState(prevState => ({ data: { ...prevState.data, distancia: val } }))}
                            />
                            <Input
                                label="Peso:"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='PESO (KG)'
                                value={this.state.data.peso}
                                keyboardType="decimal-pad"
                                defaultValue="0"
                                onChangeText={val => this.setState(prevState => ({ data: { ...prevState.data, peso: val } }))}
                            />
                            <View style={{paddingLeft:7, paddingRight:7}}>
                                <Button
                                    buttonStyle={{backgroundColor:"#FD9000", borderRadius: 100, borderColor:"#FD9000"}}
                                    titleStyle={{color:"#fff", fontFamily:"Poppins-Bold"}}
                                    title="CAPTURAR"
                                    type="outline"
                                    loading={this.state.loading}
                                    loadingProps={{color:"#CCD634"}}
                                    onPress={()=>{this.getResultado()}}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                {/*
                <View style={{position:"absolute", bottom:-50, right:0, zIndex:-3}}>
                    <Image style={{ width: 300, height: 250, resizeMode: 'contain' }} source={require('../../assets/img/triangulo-naranja.png')} />
                </View>
                */}
            </View >
        );
    }
}

const styles = StyleSheet.create({

    input:{
        backgroundColor:'#EDEDED',fontFamily:'Poppins-Bold', fontSize: 16, paddingBottom:10,paddingLeft:15, paddingRight:15, paddingTop:10,
        borderRadius: 200
    },
    container:{
        borderBottomWidth:0
    },
    superContainer:{
        marginBottom: 15
    },
    buttonHelp:{
        borderRadius:200,borderColor:"#FD9000",borderWidth:2,borderStyle:"solid", width:40, height: 40, flexDirection: "row", alignItems:"center", justifyContent: "center"
    }

});

const mapStateToProps = state => {
    return {
        WS: state.reducerWS
    };
};


export default connect(mapStateToProps)(PlanoPage)




