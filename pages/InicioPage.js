/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {Component, PureComponent} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ImageBackground, TouchableOpacity
} from 'react-native';
import {connect} from "react-redux";


class InicioPage extends Component {

    render() {
        return (
            <View style={{position:"relative", minHeight:'100%'}}>
                    <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', padding: 20}}>
                        <View style={{paddingLeft: 50, paddingRight: 50}}>
                            <Image
                                style={{width: '100%', height: 200, resizeMode: 'contain'}}
                                source={require('../assets/img/logo.png')}
                            />
                        </View>
                        <View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('PenduloPage')}>
                                <View style={styles.buttonPendulo}>
                                    <Image
                                        style={{width: 40, height: 30, resizeMode: 'contain'}}
                                        source={require('../assets/img/pendulo.png')}
                                    />
                                    <View>
                                        <Text style={{
                                            color: 'white',
                                            fontSize: 18,
                                            fontFamily: 'Poppins-Bold',
                                            marginLeft: 10
                                        }}>PÉNDULO SIMPLE</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('PlanoPage')}>
                                <View style={styles.buttonPlano}>
                                    <Image
                                        style={{width: 40, height: 30, resizeMode: 'contain'}}
                                        source={require('../assets/img/plano.png')}
                                    />
                                    <View>
                                        <Text style={{
                                            color: 'white',
                                            fontSize: 18,
                                            marginLeft: 10,
                                            fontFamily: 'Poppins-Bold'
                                        }}>PLANO INCLINADO</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('MovimientoCircularPage')}>
                                <View style={styles.buttonCircular}>
                                    <Image
                                        style={{width: 40, height: 30, resizeMode: 'contain'}}
                                        source={require('../assets/img/circular-icon.png')}
                                    />
                                    <View style={{flex: 1}}>
                                        <Text style={{
                                            color: 'white',
                                            fontSize: 18,
                                            marginLeft: 10,
                                            fontFamily: 'Poppins-Bold'
                                        }}>MOVIMIENTO CIRCULAR UNIFORME</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{textAlign: 'center', color: '#4585BA', fontFamily: 'Poppins-Bold'}}>Desarrollado
                                por Ceindetec
                                Llanos</Text>
                        </View>
                    </View>
                <View style={{position:"absolute", bottom:-220, right:-130, zIndex:-3}}>
                    <Image style={{ width: 500, height: 600, resizeMode: 'contain' }} source={require('../assets/img/Elipse.png')} />
                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    buttonPendulo: {
        width: '100%',
        backgroundColor: '#FD9000',
        padding: 15,
        elevation: 5,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: "flex-start",
        alignItems: "center",
        borderRadius: 200
    },
    buttonPlano: {
        width: '100%',
        backgroundColor: '#FD9000',
        padding: 15,
        elevation: 5,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: "flex-start",
        alignItems: "center",
        borderRadius: 200
    },
    buttonCircular: {
        width: '100%',
        backgroundColor: '#FD9000',
        padding: 15,
        elevation: 5,
        flexDirection: 'row',
        justifyContent: "flex-start",
        alignItems: "center",
        borderRadius: 200
    },
});


const mapStateToProps = state => {
    return {
        WS: state.reducerWS
    };
};

const mapDispatchToProps = dispatch => {
    return {
    };
};


export default connect(mapStateToProps,mapDispatchToProps)(InicioPage)