import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity,
    Platform, Text
} from 'react-native';
import {Icon, Button, Header, Input, Overlay} from 'react-native-elements';
import MovimientoFormulas from "./MovimientoFormulas";
import { EventRegister } from 'react-native-event-listeners';
import HTML from "react-native-render-html";
import AwesomeAlert from "react-native-awesome-alerts";

const LeftComponent = (action) => {
    return (
        <TouchableOpacity onPress={action}>
            <Image
                style={{ width: 60, height: 70, resizeMode:'contain' }}
                source={require('../../assets/img/logo-sm.png')}
            />
        </TouchableOpacity>
    );
}

const RightComponent = action => {
    return (
        <Image
            style={{ width: 50, height: 60, resizeMode: 'contain' }}
            source={require('../../assets/img/blue/circular-icon.png')}
        />
    );
}

export default class MovimientoResultado extends Component {
    state = {
        loading:false,
        mostrarResultado: false,
        mostrarFormulas: false,
        velocidadResultado:'',
        velocidadUsuario:'',
        htmlContent:``,
        showAlert: false
    };
    mostrarResultado(){
        this.setState({loading: true});
        let periodo = Number(this.props.navigation.getParam('periodo'));
        let radio = Number(this.props.navigation.getParam('radio'));
        if(isNaN(Number(periodo)) || Number(periodo) === 0){
            this.setState({
                showAlert: true
            })
        }else{
            let velocidadAngular = (2 * Math.PI * radio)/periodo;
            let htmlContent = `
            <p>
            w<sub>angular</sub> = 2&Pi;/T<br>
            v<sub>tangencial</sub> = w * r<br>
            T = ${periodo.toFixed(2)}s<br>
            w = 2&Pi;/${periodo.toFixed(2)} = ${((2*Math.PI)/periodo).toFixed(2)}<br>
            r = ${radio}m <br>
            v = ${((2*Math.PI)/periodo).toFixed(2)} * ${radio}m <br>
            v = ${((2*Math.PI)/periodo) * radio}
            </p>
         `
            let cincoPorciento = (5*velocidadAngular)/100;
            let margenError = Math.abs(velocidadAngular-this.state.velocidadUsuario).toFixed(2);
            if(margenError>cincoPorciento){
                //le quedó mal
                EventRegister.emit('customAlert', {type: 'error',margenError:margenError})
            }else{
                //le quedó bien
                EventRegister.emit('customAlert', {type:'success',margenError:margenError})
            }
            this.setState({
                mostrarResultado: true,
                loading: false,
                velocidadResultado: velocidadAngular.toString(),
                htmlContent: htmlContent
            });
        }
    }
    cerrarFormulas(){
        this.setState({mostrarFormulas: false})
    }
    render() {
        return (
            <View style={{position:"relative", minHeight:'100%'}}>
                <Overlay isVisible={this.state.mostrarFormulas}>
                    <MovimientoFormulas cerrar={()=>{this.cerrarFormulas()}} />
                </Overlay>
                <AwesomeAlert
                    show={this.state.showAlert}
                    title="Error!"
                    message="Por favor, intenta de nuevo."
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText="Cerrar"
                    alertContainerStyle={{zIndex:10}}
                    onCancelPressed={()=>{
                        this.setState({
                            showAlert: false
                        })
                        this.props.navigation.goBack()
                    }}
                />
                <Header
                    backgroundColor="#fff"
                    leftComponent={LeftComponent(()=>{this.props.navigation.navigate('MainPage')})}
                    centerComponent={{ text: 'MOVIMIENTO CIRCULAR', style:{color: '#4585BA', fontSize:16, fontFamily:'Poppins-Bold'} }}
                    rightComponent={RightComponent}
                    containerStyle={{
                        height: Platform.OS === 'ios' ? 110 : 70,
                        paddingTop:0,
                        borderBottomWidth:3,
                        borderBottomColor:"#EFEFEF"
                    }}
                />
                <ScrollView style={{flex: 1}}>
                    <View>
                        <View style={{justifyContent:'space-between',flexDirection:"row", alignItems:"center", marginBottom:2, padding: 10}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon
                                    name='arrow-back'
                                    color='black'
                                />
                            </TouchableOpacity>
                            <Text style={{fontSize:18, fontFamily:'Poppins-Bold'}}>RESULTADO</Text>
                            <TouchableOpacity onPress={()=>{this.setState({mostrarFormulas: true})}}>
                                <View style={styles.buttonHelp}>
                                    <Text style={{color:"#FD9000",fontSize:22, textAlign:"center", fontFamily:"Poppins-Regular"}}>?</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.title}>
                            <Text style={{color:"#4585BA",fontFamily:'Poppins-Bold', fontSize:16}}>DATOS CALCULADOS POR EL USUARIO: </Text>
                        </View>
                        <View style={{paddingLeft: 10, paddingRight: 10}}>
                            <Input
                                label="Velocidad tangencial:"
                                labelProps={{fontFamily:'Poppins-Bold'}}
                                labelStyle={{color:'black', fontSize:16}}
                                inputStyle={styles.input}
                                inputContainerStyle={styles.container}
                                containerStyle={styles.superContainer}
                                placeholder='Ingresar velocidad calculada'
                                disabled={this.state.mostrarResultado}
                                value={this.state.velocidadUsuario}
                                keyboardType="decimal-pad"
                                defaultValue="0"
                                onChangeText={(text)=>{this.setState({velocidadUsuario: text})}}
                            />
                        </View>
                        {
                            this.state.mostrarResultado ?
                                <View>
                                    <View style={styles.title}>
                                        <Text style={{color:"#4585BA",fontFamily:'Poppins-Bold', fontSize:16}}>DATOS OBTENIDOS: </Text>
                                    </View>
                                    <View style={{paddingLeft: 10, paddingRight: 10}}>
                                        <Input
                                            label="Velocidad tangencial:"
                                            labelProps={{fontFamily:'Poppins-Bold'}}
                                            labelStyle={{color:'black', fontSize:16}}
                                            inputStyle={styles.input}
                                            inputContainerStyle={styles.container}
                                            containerStyle={styles.superContainer}
                                            value={this.state.velocidadResultado}
                                            disabled={true}
                                        />
                                    </View>
                                    <HTML baseFontStyle={{fontSize:18,fontFamily:'Poppins-Regular'}} html={this.state.htmlContent} />
                                </View>
                                : <View style={{paddingLeft:17, paddingRight:17}}>
                                    <Button
                                        buttonStyle={{borderColor:"#FD9000", backgroundColor:"#FD9000", borderRadius: 100}}
                                        titleStyle={{color:"#fff", fontFamily:"Poppins-Bold"}}
                                        title="COMPARAR"
                                        type="outline"
                                        loading={this.state.loading}
                                        loadingProps={{color:"#fff"}}
                                        onPress={()=>{this.mostrarResultado()}}
                                    />
                                </View>
                        }
                    </View>
                </ScrollView>
            </View >
        );
    }
}

const styles = StyleSheet.create({

    input:{
        backgroundColor:'#EDEDED',fontFamily:'Poppins-Bold', fontSize: 16, paddingBottom:10,paddingLeft:15, paddingRight:15, paddingTop:10, borderRadius: 100
    },
    container:{
        borderBottomWidth:0
    },
    superContainer:{
        marginBottom: 15
    },
    buttonHelp:{
        borderRadius:200,borderColor:"#FD9000",borderWidth:2,borderStyle:"solid", width:40, height: 40, flexDirection: "row", alignItems:"center", justifyContent: "center"
    },
    title:{
        backgroundColor:"#B8DFFF",paddingTop:10, paddingBottom:10, paddingLeft:15, paddingRight:15, marginBottom: 15
    }

});




