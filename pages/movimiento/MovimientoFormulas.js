import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity,
    Platform,
    Text
} from 'react-native';
import { Button } from 'react-native-elements';
import HTML from "react-native-render-html";

const htmlContent = `
        <p>
        <span style="font-size: 25px">w</span><sub>angular</sub> = 2&Pi;/T<br>
        <span style="font-size: 25px">v</span><sub>tangencial</sub> = w * r
        </p>
`

export default class MovimientoFormulas extends Component {

    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {
        return (
            <View style={{padding: 10, flex: 1, flexDirection: "column", justifyContent: 'space-between'}}>
                <View>
                    <Text style={{fontSize:18, fontFamily:'Poppins-Bold', textAlign: 'center', marginBottom: 10}}>FÓRMULAS MOVIMIENTO CIRCULAR UNIFORME</Text>
                    <HTML baseFontStyle={{fontSize:18,fontFamily:'Poppins-Regular'}} html={htmlContent} />
                </View>
                <Button
                    buttonStyle={{borderColor:"#4585BA", borderWidth:2}}
                    titleStyle={{color:"#4585BA", fontFamily:"Poppins-Bold"}}
                    title="CERRAR"
                    type="outline"
                    loading={this.state.loading}
                    loadingProps={{color:"#4585BA"}}
                    onPress={()=>{this.props.cerrar()}}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({


});




