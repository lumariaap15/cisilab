import { takeEvery, call, select, put, all } from "redux-saga/effects";
import { AsyncStorage } from "react-native";
import {actionDisconnectSocket, actionGuarrdarSocket, actionMessageSocket} from "./actions";


function* conectaWS(ws) {
   yield put(actionGuarrdarSocket(ws));
}

function* disconnectWS(ws) {
    yield put(actionDisconnectSocket(ws));
}

function* messageWS(message) {
    message = JSON.parse(message.message);
    yield put(actionMessageSocket(message));
}

function* funcionPrimaria() {
    yield takeEvery('SAGA_SOCKET_CONNET', conectaWS);
    yield takeEvery('SAGA_SOCKET_DISCONNECT', disconnectWS);
    yield takeEvery('SAGA_SOCKET_MESSAGE', messageWS);
}

export default funcionPrimaria;
