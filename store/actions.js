

export const actionGuarrdarSocket = ws => ({
    type: 'SOCKET_CONNET',
    ws
});

export const actionDisconnectSocket = () => ({
    type: 'SOCKET_DISCONNECT'
});

export const actionMessageSocket = (message) => ({
    type: 'SOCKET_MESSAGE',
    message
});