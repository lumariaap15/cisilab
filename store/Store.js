import {createStore, combineReducers, applyMiddleware} from "redux";
import createSagaMiddleware from "redux-saga";
import funcionPrimaria from "./Saga";


const initialState = {
    ws: null,
    estado: false,
    message: {
        modulo: '',
        segundos: '',
        milisegundos: '',
        inclinacion: '',
        numerogiros: '',
        oscilaciones: ''
    }
};


export const reducerWS = (state = initialState, action) => {
    switch (action.type) {
        case 'SOCKET_CONNET':
            return {
                ws: action.ws,
                estado: true,
                message: state.message
            };
        case 'SOCKET_DISCONNECT':
            return {
                ws: null,
                estado: false,
                message: {
                    modulo: '',
                    segundos: '',
                    milisegundos: '',
                    inclinacion: '',
                    numerogiros: '',
                    oscilaciones: ''
                }
            };
        case 'SOCKET_MESSAGE':
            return {
                ws: state.ws,
                estado: state.estado,
                message: action.message
            };
        default:
            return state
    }
};

const reducer = combineReducers({
    reducerWS
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(funcionPrimaria);


export default store;
