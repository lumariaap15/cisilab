import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import {connect} from "react-redux";
import {reducerWS} from "../store/Store";


class WebSocketConet extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ws: null
        };
    }

    componentDidMount() {
        this.connect();
    }

    timeout = 250; // Initial timeout duration as a class variable

    /**
     * @function connect
     * This function establishes the connect with the websocket and also ensures constant reconnection if connection closes
     */
    connect = () => {
        console.log('conectando');
        var ws = new WebSocket("ws://192.168.4.1:81/ws");
        //var ws = new WebSocket("ws://192.168.0.6:9898/ws");
        let that = this; // cache the this
        var connectInterval;

        ws.onopen = () => {
            console.log("connected websocket main component");
            this.setState({ws: ws});
            that.timeout = 250; // reset timer to 250 on open of websocket connection
            clearTimeout(connectInterval); // clear Interval on on open of websocket connection
            that.props.conectarWS(ws);
        };

        ws.onclose = e => {
            console.log(
                `Socket is closed. Reconnect will be attempted in ${Math.min(
                    10000 / 1000,
                    (that.timeout + that.timeout) / 1000
                )} second.`,
                e.reason
            );
            ws.close();
            that.props.disconnectWS();
            that.timeout = that.timeout + that.timeout; //increment retry interval
            connectInterval = setTimeout(this.check, Math.min(10000, that.timeout)); //call check function after timeout
        };

        ws.onerror = err => {
            console.log(err);
            ws.close();
            that.props.disconnectWS();
        };

        ws.onmessage = message => {
            message.data = message.data.replace(/'/g, '"');
            console.log(message.data);
            that.props.messageWS(message.data);
        }
    };

    check = () => {
        const {ws} = this.state;
        if (!ws || ws.readyState == WebSocket.CLOSED) this.connect(); //check if websocket instance is closed, if so call `connect` function.
    };

    render() {
        return (
            <View></View>
        )
    }

}


const mapStateToProps = state => {
    return {
        WS: state.reducerWS
    };
};

const mapDispatchToProps = dispatch => {
    return {
        conectarWS: (ws) => {
            dispatch({
                type:'SAGA_SOCKET_CONNET',
                ws
            });
        },
        disconnectWS: () => {
            dispatch({
                type: 'SAGA_SOCKET_DISCONNECT'
            })
        },
        messageWS: (message) => {
            dispatch({
                type: 'SAGA_SOCKET_MESSAGE',
                message
            })
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(WebSocketConet)




