import { createAppContainer } from "react-navigation";
import { fromRight } from 'react-navigation-transitions';
import InicioPage from "../pages/InicioPage";
import { createStackNavigator } from 'react-navigation-stack'
import PenduloPage from "../pages/pendulo/PenduloPage";
import PlanoPage from "../pages/plano/PlanoPage";
import MovimientoCircularPage from "../pages/movimiento/MovimientoCircularPage";
import PenduloResultado from "../pages/pendulo/PenduloResultado";
import PlanoResultado from "../pages/plano/PlanoResultado";
import MovimientoResultado from "../pages/movimiento/MovimientoResultado";


const StackHome = createStackNavigator(
    {
        MainPage: {
            screen: InicioPage
        },
        PenduloPage: {
            screen: PenduloPage
        },
        PenduloResultado:{
            screen: PenduloResultado
        },
        PlanoPage: {
            screen: PlanoPage
        },
        PlanoResultado:{
          screen: PlanoResultado
        },
        MovimientoCircularPage: {
            screen: MovimientoCircularPage
        },
        MovimientoResultado:{
            screen: MovimientoResultado
        },
    },
    {
        defaultNavigationOptions: {
            header: null
        },
        transitionConfig: () => fromRight(),
    }
);

export default createAppContainer(StackHome);
