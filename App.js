/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {PureComponent} from 'react';
import {
    StyleSheet, Text,
    View,
} from 'react-native';
import NavigationComponent from './components/NavigationComponent';
import {Provider} from "react-redux";
import store from "./store/Store";
import WebSocketConet from "./components/WebSocketConet";
import {EventRegister} from "react-native-event-listeners";
import Toast,{DURATION} from "react-native-easy-toast";



class App extends PureComponent{
    componentDidMount() {
        this.listener = EventRegister.on('customAlert', (data) => {
            if(data.type === 'success'){
                let message = `¡Bien! Tu estimación fue correcta, tuviste un margen de error de ${data.margenError}.`
                this.refs.successToast.show(
                    <View>
                        <Text style={{color:'#3CCC2C', fontFamily:'Poppins-Bold', fontSize: 16}}>{message}</Text>
                    </View>
                    ,6000);
            }else if(data.type === 'error'){
                let message = `¡Error! Lo sentimos, tus cálculos no fueron correctos, tuviste un margen de error de ${data.margenError}.`
                this.refs.dangerToast.show(<View><Text style={{color:'#FF3D43', fontFamily:'Poppins-Bold', fontSize: 16}}>{message}</Text></View>,6000);
            }
        })
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(this.listener)
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Provider store={store}>
                    <WebSocketConet/>
                    <NavigationComponent/>
                </Provider>
                <Toast ref="successToast"
                       style={{backgroundColor:'#CAE5C7'}}
                       position='bottom'
                       positionValue={100}
                       fadeInDuration={750}
                       fadeOutDuration={1000}
                       opacity={0.8}/>
                <Toast ref="dangerToast"
                       style={{backgroundColor:'#F6C9C9'}}
                       position='bottom'
                       positionValue={150}
                       fadeInDuration={750}
                       fadeOutDuration={1000}
                       opacity={0.8}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({});

export default App;